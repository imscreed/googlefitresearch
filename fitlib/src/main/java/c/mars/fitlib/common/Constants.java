package c.mars.fitlib.common;

/**
 * Created by Constantine Mars on 1/19/2015.
 */
public class Constants {
    public static final String FRAG_ID = "FRAG_ID";
    public static final int NO_ID = -1;

    public static class Action {
        public static final String ADD = "ADD";
        public static final String ADD_ALL = "ADD_ALL";
        public static final String CLEAR = "CLEAR";
    }
    public static final String DATA = "DATA";
}
